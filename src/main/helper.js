const {v4:uuid} = require('uuid');
//function to query limit_service_db
const {getTimeFromDb} = require('./db_query')

let transactions = [
  {
    id: uuid(),
    remark: 'Starbucks',
    cardId: '1234',
    amount: '10.99'
  },
  {
    id: uuid(),
    remark: 'amazon.com',
    cardId: '5678',
    amount: '140.49'
  },
];

const getAllTransactions = () => {
  return transactions;
}

const getCurrentTime = () => {
  return new Date().toISOString();
}

const getCurrentTimeFromDb = async () => {
  //querying db for current time
  const queryResult = await getTimeFromDb();
  console.log(queryResult);
  return queryResult.rows[0].asoftime;
}

module.exports = {getCurrentTime, getAllTransactions,getCurrentTimeFromDb}