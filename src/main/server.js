const app = require("./app");
const {getCurrentTime} = require('./helper.js')

app.listen(3000, () =>
    console.log('DTA (Node Express and Postgres) app is listening on port 3000 since ' + getCurrentTime()),
);