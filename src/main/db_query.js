//function for setting up database connection
const {getPostgresClient} = require('./db_connect.js')

const getTimeFromDb = async ()=>{

    //establishing connection with db
    const client = getPostgresClient();
    const res = await client.query(
        `SELECT now() AS asOfTime`
      );
    return res;  
}

module.exports = {getTimeFromDb}