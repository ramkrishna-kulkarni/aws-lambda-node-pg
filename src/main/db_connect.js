const { Client } = require('pg');
const dotenv = require('dotenv');
dotenv.config();

const getPostgresClient = ()=>{
    const client = new Client();    
    client.connect();
    return client;
}

module.exports = {getPostgresClient}