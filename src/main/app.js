const bodyParser = require('body-parser');
const express = require('express');
const {getCurrentTime, getAllTransactions, getCurrentTimeFromDb} = require('./helper.js')

const app = express();
app.use(bodyParser.json({ type: 'application/json' }));

app.get('/',(req,res) =>
{
  let welcomeMessage = 'Thank you for visiting us at the time ' + getCurrentTime();
  res.send('Warm Welcome! ' + "\n" + welcomeMessage);
});

app.get('/transactions', (req,res) =>
{
  return res.json(getAllTransactions());
});

app.post('/authorizeTran', (req, res) => {
  const transactionType = req.body.transaction_type;
  const transactionId = req.body.transaction_id;
  if (transactionType !== 'MC') {
    res.statusCode = 500;
    console.log("Could not authorize transaction " + transactionId + " made by team card as max limit has been reached")
    return res.json({
      errors: ['Could not authorize transaction as max limit has been reached']
    });
  }
  res.statusCode = 200;
  console.log("Transaction " + transactionId + " made by team card authorized successfully");
  return res.send('Transaction made by team card authorized successfully');
});

app.get('/db', async (req,res) =>
{
  const timeFromDb = await getCurrentTimeFromDb();
  const welcomeMessage = 'Thank you for visiting us at the time ' + timeFromDb;
  res.send('Warm Welcome! ' + "\n" + welcomeMessage);
});


module.exports = app;