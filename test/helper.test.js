const {getCurrentTime, getAllTransactions, getCurrentTimeFromDb} = require('../src/main/helper.js')
const {v4:uuid} = require('uuid');

describe('Helper Unit tests', () => {

    describe('Test the generation of current time from code', function(){

        test('Should return current time', function(){
            const mockDate = new Date('14 Oct 1995');
            global.Date = jest.fn().mockImplementation(() => mockDate);      
            var result = getCurrentTime();
            expect(result).toBe(mockDate.toISOString()); 
        });

    });

    describe('Test fetching of transactions', function(){

        test('should return transactions json object', function(){
            var expectedVal = [
                {
                  id: uuid(),
                  remark: 'Starbucks',
                  cardId: '1234',
                  amount: '10.99'
                },
                {
                  id: uuid(),
                  remark: 'amazon.com',
                  cardId: '5678',
                  amount: '140.49'
                },
              ];
            var result = getAllTransactions();
            expect(result[0].remark).toBe(expectedVal[0].remark); 
            expect(result[0].cardId).toBe(expectedVal[0].cardId);
            expect(result[0].amount).toBe(expectedVal[0].amount);
        });

    });
    
    

});    