
const { Client } = require('pg')
//const {} = require('dotenv')
const { getPostgresClient } = require('../src/main/db_connect')

jest.mock('pg', () => {
  const mClient = {
    connect: jest.fn(),
  };
  return { Client: jest.fn(() => mClient) };
});

describe('Test getPostgresClient', () => {
  let client;
  beforeEach(() => {
    client = new Client();
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('should success',  () => {
    let resultClient = getPostgresClient();
    expect(JSON.stringify(resultClient)).toBe(JSON.stringify(client));
    expect(client.connect).toBeCalledTimes(1);
  });
})