// we will use supertest to test HTTP requests/responses
const request = require("supertest");
// we also need our app for the correct routes!
const app = require("../src/main/app");

describe("Test the root path", () => {
  test("It should response the GET method", done => {
    request(app)
    .get("/")
    .then(response => {
      expect(response.statusCode).toBe(200);
      done();
    });
  });
});

describe("Test the path /transactions", () => {
  test("It should respond with an array of transactions", done => {
    request(app)
    .get("/transactions")
    .then(response => {
      expect(response.body.length).toEqual(2);
      expect(response.statusCode).toBe(200);
      done();
    });
  });
});