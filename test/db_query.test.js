const { getTimeFromDb } = require('../src/main/db_query')

const mockExpectedTime = "mock value";
jest.mock('../src/main/db_connect', () => {
  return {
    getPostgresClient : function () {
      return {
        query :  jest.fn(() => mockExpectedTime),
      }
    },
  };
})

describe('Test getTimeFromDb',  () => {

  it('should success getTimeFromDb',   async () => {
    const time = await getTimeFromDb();
    expect(time).toBe(mockExpectedTime);
  });
})


